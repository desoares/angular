angular.module('listaTelefonica').factory('contatosAPI', function ($http, config) {
    var _getContatos = function () {
            return $http.get(config.HOST + "/contatos");
        },
        _saveContato = function (contato) {
            return $http.post(config.HOST + '/contatos', contato);
        };

     return {
        'getContatos': _getContatos,
        'saveContato': _saveContato
     };
});