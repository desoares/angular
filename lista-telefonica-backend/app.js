'use strict';
var express = require('express'),
      bodyParser = require('body-parser'),
      app = express(),
      fs = require('fs');

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());

var contatos = require('./contatos.json');
var operadoras =  require('./operadoras.json');

app.listen(8000, function() {
  console.log('Up and running!');
});

app.all('*', function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

app.get('/contatos', function(req, res) {
   console.log('Getting contatos');
  return res.status(200).json(contatos);
});

app.post('/contatos', function(req, res) {
  contatos.push(req.body);
  fs.writeFile('./contatos.json', JSON.stringify(contatos), function (err) {
  });

  res.status(200).json(req.body);
});

app.get('/operadoras', function(req, res) {
  res.status(200).json(operadoras);
});