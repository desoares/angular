angular.module("listaTelefonica").controller("listaTelefonicaCtrl", function ($scope, contatosAPI, operadorasAPI) {
                 $scope.contatos = [];
                $scope.app = "Lista Telefonica";

                contatosAPI.getContatos().success(function (data, status) {
                        $scope.contatos = data;
                });

                operadorasAPI.getOperadoras().success(function(data, status) {
                    $scope.operadoras = data;
                });

                $scope.adicionaContato = function (contato) {
                    contato.data = new Date();
                    contatosAPI.saveContato(contato).success(function (data, status) {
                        delete $scope.contato;
                        $scope.contatoForm.$setPristine();
                        $scope.contatos.push(data);
                    }).error(function (err, status) {
                         $scope.postErrorMessage =  "Erro ao salvar contato: " + err + ". Status: " + status;
                    });
                };

                $scope.excluiContatos = function (contatos) {
                    $scope.contatos = contatos.filter(function (contato) {
                        if (!contato.selecionado)
                        {
                            return contato
                        }
                    });
                };

                $scope.isContatoSelecionado = function (contatos) {
                    return contatos.some(function (contato) {
                        return contato.selecionado;
                    });
                };

                $scope.ordenarPor  = function (campo) {
                    $scope.criteria = campo;
                    $scope.desc = !$scope.desc;
                }
            });